﻿/* ----------------------------------------------
 * 
 * 				#PROJECTNAME#
 * 
 * Original Author: Abela Paolo
 * Creation Date: #CREATIONDATE#
 * Updates: 
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
using System.Collections;

///<summary>
///
///</summary>

public class Giocatore : MonoBehaviour
{
    public static Giocatore singleton;
    int punteggioCorrente;

    void Awake()
    {
        if ((singleton != null) && (singleton != this))
        {
            Destroy(gameObject);
            return;
        }
        singleton = this;
        punteggioCorrente = 0;
    }

    public void Saluta()
    {
        Debug.Log("Ciaoooooo", gameObject);
    }

    public void OttieniPunti(int punti)
    {
        punteggioCorrente = punteggioCorrente + punti;
        Debug.Log("Punteggio: " + punteggioCorrente);
    }
}
