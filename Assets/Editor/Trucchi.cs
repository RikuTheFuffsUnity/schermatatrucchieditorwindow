﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Trucchi : EditorWindow
{
    const string PREFISSO_OPZIONI = "trucchi.";
    bool mostraPulsante;
    float puntiDaDare = 0;

    [MenuItem("RikuTheFuffs/Trucchi")]
    public static void ShowWindow()
    {
        GetWindow(typeof(Trucchi));
    }

    void OnGUI()
    {
        GUILayout.FlexibleSpace();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Punti da dare: ");
        puntiDaDare = GUILayout.HorizontalSlider(puntiDaDare, 0, 100);
        GUILayout.Label("" + Mathf.Round(puntiDaDare));
        GUILayout.EndHorizontal();

        mostraPulsante = EditorGUILayout.Toggle("Mostra pulsante", mostraPulsante, EditorStyles.toggle);
        if (mostraPulsante && EditorApplication.isPlaying)
        {
            if (GUILayout.Button("Dai punti"))
            {
                AssegnaPunti();
            }
        }
        GUILayout.FlexibleSpace();
    }

    void AssegnaPunti()
    {
        Debug.Log("mi hai premuto!");
        Giocatore.singleton.OttieniPunti(Mathf.RoundToInt(puntiDaDare));
    }

    void Awake()
    {
        CaricaPreferenze();
    }

    void OnFocus()
    {
        CaricaPreferenze();
    }

    void OnLostFocus()
    {
        SalvaPreferenze();
    }

    void OnDestroy()
    {
        SalvaPreferenze();
    }

    void SalvaPreferenze()
    {
        EditorPrefs.SetFloat(string.Format("{0}puntiDaDare", PREFISSO_OPZIONI), puntiDaDare);
        EditorPrefs.SetBool(string.Format("{0}mostraPulsante", PREFISSO_OPZIONI), mostraPulsante);
    }

    void CaricaPreferenze()
    {
        puntiDaDare = EditorPrefs.GetFloat(string.Format("{0}puntiDaDare", PREFISSO_OPZIONI), 0);
        mostraPulsante = EditorPrefs.GetBool(string.Format("{0}mostraPulsante", PREFISSO_OPZIONI), true);
    }
}
